#/usr/bin/env bash

asdf update
asdf plugin-list | grep -e ^rust$ | false && asdf plugin add rust
asdf plugin-list | grep -e ^rust-analyzer$ | false && asdf plugin add rust-analyzer
asdf install
cargo --version
